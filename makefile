# makefile for opengl single file app

ALLAPP = $(subst .c,,$(subst src/,,$(wildcard src/*.c)))
ALLAPP += $(subst .c,,$(wildcard *.c))

TARGET ?= main
TGTLBL = app
OBJLST = $(TARGET).o

CC = gcc

DELETE = rm -rf

CFLAGS += -Wall
LFLAGS += $(LDFLAGS) -lGL -lGLU -lglut -lm
OFLAGS +=
XFLAGS += -D_LARGEFILE_SOURCE=1 -D_FILE_OFFSET_BITS=64

.PHONY: dummy $(TARGET)

debug: CFLAGS += -DMY1_DEBUG

$(TARGET): $(OBJLST)
	$(CC) $(CFLAGS) -o $(TGTLBL) $^ $(LFLAGS) $(OFLAGS)

dummy:
	@echo "Run 'make <app>'"
	@echo "  <app> = { $(ALLAPP) }"

debug: $(TARGET)

%: %.c
	$(CC) $(CFLAGS) -o $@ $< $(LFLAGS) $(OFLAGS)

clean:
	-$(DELETE) $(ALLAPP) $(TGTLBL) *.o
