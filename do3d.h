/*----------------------------------------------------------------------------*/
#ifndef __MY1WORK3D__
#define __MY1WORK3D__
/*----------------------------------------------------------------------------*/
#include "quat.h"
#include "vec3.h"
/*----------------------------------------------------------------------------*/
void zero_transform(float* data) {
	int loop, irow, icol;
	for (loop=0,irow=0;irow<4;irow++)
		for (icol=0;icol<4;icol++,loop++)
			data[loop] = irow == icol ? 1.0f : 0.0f;
}
/*----------------------------------------------------------------------------*/
#define _USE_COL_MAJOR_
#ifdef _USE_COL_MAJOR_
#define SIZE 4
#define R0C0 0
#define R0C1 (R0C0+SIZE)
#define R0C2 (R0C1+SIZE)
#define R0C3 (R0C2+SIZE)
#define R1C0 (R0C0+1)
#define R1C1 (R1C0+SIZE)
#define R1C2 (R1C1+SIZE)
#define R1C3 (R1C2+SIZE)
#define R2C0 (R1C0+1)
#define R2C1 (R2C0+SIZE)
#define R2C2 (R2C1+SIZE)
#define R2C3 (R2C2+SIZE)
#define R3C0 (R2C0+1)
#define R3C1 (R3C0+SIZE)
#define R3C2 (R3C1+SIZE)
#define R3C3 (R3C2+SIZE)
#else /** _USE_ROW_MAJOR_ */
#define SIZE 4
#define R0C0 0
#define R0C1 (R0C0+1)
#define R0C2 (R0C0+2)
#define R0C3 (R0C0+3)
#define R1C0 (R0C0+SIZE)
#define R1C1 (R1C0+1)
#define R1C2 (R1C0+2)
#define R1C3 (R1C0+3)
#define R2C0 (R1C0+SIZE)
#define R2C1 (R2C0+1)
#define R2C2 (R2C0+2)
#define R2C3 (R2C0+3)
#define R3C0 (R2C0+SIZE)
#define R3C1 (R3C0+1)
#define R3C2 (R3C0+2)
#define R3C3 (R3C0+3)
#endif
/*----------------------------------------------------------------------------*/
void quat_make_rotation(quat_t* quat, float* data) {
	/* row 0 */
	data[R0C0] = 1.0f - 2.0f*quat->y*quat->y - 2.0f*quat->z*quat->z;
	data[R0C1] = 2.0f*quat->x*quat->y - 2.0f*quat->z*quat->w;
	data[R0C2] = 2.0f*quat->x*quat->z + 2.0f*quat->y*quat->w;
	data[R0C3] = 0.0f;
	/* row 1 */
	data[R1C0] = 2.0f*quat->x*quat->y + 2.0f*quat->z*quat->w;
	data[R1C1] = 1.0f - 2.0f*quat->x*quat->x - 2.0f*quat->z*quat->z;
	data[R1C2] = 2.0f*quat->y*quat->z - 2.0f*quat->x*quat->w;
	data[R1C3] = 0.0f;
	/* row 2 */
	data[R2C0] = 2.0f*quat->x*quat->z - 2.0f*quat->y*quat->w;
	data[R2C1] = 2.0f*quat->y*quat->z + 2.0f*quat->x*quat->w;
	data[R2C2] = 1.0f - 2.0f*quat->x*quat->x - 2.0f*quat->y*quat->y;
	data[R2C3] = 0.0f;
	/* row 3 */
	data[R3C0] = 0.0f;
	data[R3C1] = 0.0f;
	data[R3C2] = 0.0f;
	data[R3C3] = 1.0f;
}
/*----------------------------------------------------------------------------*/
void quat_from_rotation(quat_t* quat, float* data) {
	float invs, temp = data[R0C0]+data[R1C1]+data[R2C2];
	if (temp>0.0f) {
		invs = inv_sqrt((1.0f+temp)*4.0f);
		quat->w = 0.25f/invs;
		quat->x = (data[R2C1]-data[R1C2])*invs;
		quat->y = (data[R0C2]-data[R2C0])*invs;
		quat->z = (data[R1C0]-data[R0C1])*invs;
	}
	else if ((data[R0C0]>data[R1C1])&&(data[R0C0]>data[R2C2])) {
		temp = data[R0C0]-data[R1C1]-data[R2C2];
		invs = inv_sqrt((1.0f+temp)*4.0f);
		quat->w = (data[R2C1]-data[R1C2])*invs;
		quat->x = 0.25f/invs;
		quat->y = (data[R0C1]+data[R1C0])*invs;
		quat->z = (data[R0C2]+data[R2C0])*invs;
	}
	else if (data[R1C1]>data[R2C2]) {
		temp = data[R1C1]-data[R0C0]-data[R2C2];
		invs = inv_sqrt((1.0f+temp)*4.0f);
		quat->w = (data[R0C2]-data[R2C0])*invs;
		quat->x = (data[R0C1]+data[R1C0])*invs;
		quat->y = 0.25f/invs;
		quat->z = (data[R1C2]+data[R2C1])*invs;
	}
	else {
		temp = data[R2C2]-data[R0C0]-data[R1C1];
		invs = inv_sqrt((1.0f+temp)*4.0f);
		quat->w = (data[R1C0]-data[R0C1])*invs;
		quat->x = (data[R0C2]+data[R2C0])*invs;
		quat->y = (data[R1C2]+data[R2C1])*invs;
		quat->z = 0.25f/invs;
	}
}
/*----------------------------------------------------------------------------*/
/** not used for now... keeping this! */
void vec3_rotate(vec3_t* vec3, vec3_t* orig, quat_t* quat) {
	vec3_t temp, *qtemp;
	/* point to vector in quat */
	qtemp = (vec3_t*) quat;
	/* first term */
	vec3_fill(vec3,qtemp->x,qtemp->y,qtemp->z);
	vec3_scale(vec3,2.0f*vec3_prod_dot(qtemp,orig));
	/* second term */
	vec3_fill(&temp,orig->x,orig->y,orig->z);
	vec3_scale(&temp,(quat->w*quat->w)-vec3_prod_dot(qtemp,qtemp));
	/* accumulate */
	vec3_sums(vec3,vec3,&temp);
	/* third term */
	vec3_prod_cross(&temp,qtemp,vec3);
	vec3_scale(&temp,2.0f*quat->w);
	/* accumulate */
	vec3_sums(vec3,vec3,&temp);
}
/*----------------------------------------------------------------------------*/
#define _3DXS_X 1
#define _3DXS_Y 2
#define _3DXS_Z 3
/*----------------------------------------------------------------------------*/
void rotate_axis(quat_t* that, int axis, float diff) {
	quat_t temp;
	/* create rotation quaternion */
	switch (axis) {
		case _3DXS_X: quat_make(&temp,0.0f,0.0f,diff); break;
		case _3DXS_Y: quat_make(&temp,0.0f,diff,0.0f); break;
		case _3DXS_Z: quat_make(&temp,diff,0.0f,0.0f); break;
	}
	quat_norm(&temp);
	quat_product(that,that,&temp);
	quat_norm(that);
}
/*----------------------------------------------------------------------------*/
void move_that(vec3_t* that, int axis, float diff) {
	switch (axis) {
		case _3DXS_X: that->x += diff; break;
		case _3DXS_Y: that->y += diff; break;
		case _3DXS_Z: that->z += diff; break;
	}
}
/*----------------------------------------------------------------------------*/
/* literal is double by default! */
#define MATH_PI 3.14159f
#define DEG2RAD(x) (x*MATH_PI/180.0f)
#define RAD2DEG(x) (x*180.0f/MATH_PI)
/*----------------------------------------------------------------------------*/
#define VIEW_DIST 10.0f
#define AXIS_SIZE (VIEW_DIST/2)
/*----------------------------------------------------------------------------*/
typedef struct _do3d_item_t {
	quat_t curr; /* current object orientation */
	vec3_t diff; /* current object translation */
	float axis_size;
	void (*make)(struct _do3d_item_t*);
	void (*draw_item)(struct _do3d_item_t*);
	void (*draw_axis)(struct _do3d_item_t*);
} do3d_item_t;
/*----------------------------------------------------------------------------*/
void do3d_item_make(do3d_item_t* item) {
	GLfloat transform[16];
	if (item->draw_axis) item->draw_axis(item);
	glPushMatrix();
	glTranslatef(item->diff.x,item->diff.y,item->diff.z);
	quat_make_rotation(&item->curr,transform);
	glMultMatrixf(transform);
	if (item->draw_item) item->draw_item(item);
	glPopMatrix();
}
/*----------------------------------------------------------------------------*/
void do3d_item_init(do3d_item_t* item) {
	quat_init(&item->curr);
	vec3_fill(&item->diff,0.0f,0.0f,0.0f);
	item->axis_size = AXIS_SIZE;
	item->make = do3d_item_make;
	item->draw_item = 0x0;
	item->draw_axis = 0x0;
}
/*----------------------------------------------------------------------------*/
void do3d_item_redo(do3d_item_t* item) {
	quat_init(&item->curr);
	vec3_fill(&item->diff,0.0f,0.0f,0.0f);
}
/*----------------------------------------------------------------------------*/
void do3d_item_move(do3d_item_t* item, int axis, float diff) {
	move_that(&item->diff,axis,diff);
	glutPostRedisplay();
}
/*----------------------------------------------------------------------------*/
void do3d_item_rotate(do3d_item_t* item, int axis, float diff) {
	rotate_axis(&item->curr,axis,DEG2RAD(diff));
	glutPostRedisplay();
}
/*----------------------------------------------------------------------------*/
typedef struct _do3d_view_t {
	quat_t view; /* view rotation */
	vec3_t vpos; /* view position */
	void (*make)(struct _do3d_view_t*);
} do3d_view_t;
/*----------------------------------------------------------------------------*/
void do3d_view_make_common(do3d_view_t* view) {
	GLfloat transform[16];
/** alternative method to translate
	zero_transform(transform);
	transform[12] = view->vpos.x;
	transform[13] = view->vpos.y;
	transform[14] = view->vpos.z;
	glMultMatrixf(transform);
*/
	glTranslatef(view->vpos.x,view->vpos.y,view->vpos.z);
	quat_make_rotation(&view->view,transform);
	/* alternative: add translation here! :) */
	glMultMatrixf(transform);
}
/*----------------------------------------------------------------------------*/
void do3d_view_init(do3d_view_t* view) {
	quat_init(&view->view);
	vec3_fill(&view->vpos,0.0f,0.0f,-VIEW_DIST);
	view->make = do3d_view_make_common;
}
/*----------------------------------------------------------------------------*/
void do3d_view_redo(do3d_view_t* view) {
	quat_init(&view->view);
	vec3_fill(&view->vpos,0.0f,0.0f,-VIEW_DIST);
}
/*----------------------------------------------------------------------------*/
void do3d_view_make(do3d_view_t* view) {
	view->make(view);
}
/*----------------------------------------------------------------------------*/
void do3d_view_move(do3d_view_t* view, int axis, float diff) {
	move_that(&view->vpos,axis,diff);
	glutPostRedisplay();
}
/*----------------------------------------------------------------------------*/
void do3d_view_rotate(do3d_view_t* view, int axis, float diff) {
	rotate_axis(&view->view,axis,DEG2RAD(diff));
	glutPostRedisplay();
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1WORK3D__ */
/*----------------------------------------------------------------------------*/
