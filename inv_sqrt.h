/*----------------------------------------------------------------------------*/
#ifndef __FAST_INV_SQRT__
#define __FAST_INV_SQRT__
/*----------------------------------------------------------------------------*/
/* on small controllers c compiler, int is 16bit! so, use long instead! */
#ifdef _CPU16B_
typedef long int32;
#else
typedef int int32;
#endif
/*----------------------------------------------------------------------------*/
/** Fast inverse square-root
 *  From: http://en.wikipedia.org/wiki/Fast_inverse_square_root
**/
float inv_sqrt(float value)
{
	float vhalf, *vtemp;
	int32 itemp, *ptemp;
	/* use int operations - faster! int size must be 32bits (float size) */
	ptemp = (int32*)&value;
	itemp = *ptemp >> 1;
	itemp = 0x5f3759df - itemp;
	/* back to floating point */
	vtemp = (float*)&itemp;
	vhalf = 0.5f * value;
	value = (*vtemp);
	value *= (1.5f - (vhalf * value * value));
	return value;
}
/*----------------------------------------------------------------------------*/
#endif /** __FAST_INV_SQRT__ */
/*----------------------------------------------------------------------------*/
