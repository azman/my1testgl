/*----------------------------------------------------------------------------*/
#ifndef __MY1QUATERNION__
#define __MY1QUATERNION__
/*----------------------------------------------------------------------------*/
/* needed by quat_make */
#include <math.h>
/*----------------------------------------------------------------------------*/
/* needed by quat_norm */
#include "inv_sqrt.h"
/*----------------------------------------------------------------------------*/
/* quat_t* now same as vec3_t* ??? that IS useful... right? */
typedef struct _quaternion_t {
	float x,y,z,w;
} quaternion_t;
typedef quaternion_t quat_t;
/*----------------------------------------------------------------------------*/
void quat_init(quat_t* quat) {
	quat->w = 1.0;
	quat->x = 0.0;
	quat->y = 0.0;
	quat->z = 0.0;
}
/*----------------------------------------------------------------------------*/
void quat_copy(quat_t* quat, quat_t* copy) {
	quat->w = copy->w;
	quat->x = copy->x;
	quat->y = copy->y;
	quat->z = copy->z;
}
/*----------------------------------------------------------------------------*/
void quat_conjugate(quat_t* quat, quat_t* from) {
	quat->w = from->w;
	quat->x = -from->x;
	quat->y = -from->y;
	quat->z = -from->z;
}
/*----------------------------------------------------------------------------*/
void quat_sum(quat_t* quat, quat_t* opr1, quat_t* opr2) {
	quat->w = opr1->w + opr2->w;
	quat->x = opr1->x + opr2->x;
	quat->y = opr1->y + opr2->y;
	quat->z = opr1->z + opr2->z;
}
/*----------------------------------------------------------------------------*/
void quat_scale(quat_t* quat, float scale) {
	quat->w *= scale;
	quat->x *= scale;
	quat->y *= scale;
	quat->z *= scale;
}
/*----------------------------------------------------------------------------*/
float quat_product_dot(quat_t* quat) {
	return quat->w*quat->w + quat->x*quat->x +
		quat->y*quat->y + quat->z*quat->z;
}
/*----------------------------------------------------------------------------*/
void quat_product(quat_t* quat, quat_t* opr1, quat_t* opr2) {
	/* note: multiplying q1 with q2 applies the rotation q2 to q1 */
	quat->w = opr1->w * opr2->w - opr1->x * opr2->x -
		opr1->y * opr2->y - opr1->z * opr2->z;
	quat->x = opr1->w * opr2->x + opr1->x * opr2->w +
		opr1->y * opr2->z - opr1->z * opr2->y;
	quat->y = opr1->w * opr2->y + opr1->y * opr2->w +
		opr1->z * opr2->x - opr1->x * opr2->z;
	quat->z = opr1->w * opr2->z + opr1->z * opr2->w +
		opr1->x * opr2->y - opr1->y * opr2->x;
}
/*----------------------------------------------------------------------------*/
void quat_make(quat_t* quat, float yaw, float pitch, float roll) {
	/* yaw (z), pitch (y), roll (x) - in radians */
	float cy = (float)cos(0.5*yaw);
	float sy = (float)sin(0.5*yaw);
	float cp = (float)cos(0.5*pitch);
	float sp = (float)sin(0.5*pitch);
	float cr = (float)cos(0.5*roll);
	float sr = (float)sin(0.5*roll);
	quat->w = cy * cp * cr + sy * sp * sr;
	quat->x = cy * cp * sr - sy * sp * cr;
	quat->y = sy * cp * sr + cy * sp * cr;
	quat->z = sy * cp * cr - cy * sp * sr;
}
/*----------------------------------------------------------------------------*/
void quat_norm(quat_t* quat) {
	float sum_sq = quat->w*quat->w + quat->x*quat->x +
		quat->y*quat->y + quat->z*quat->z;
	float inv_sq = inv_sqrt(sum_sq);
	quat->w *= inv_sq;
	quat->x *= inv_sq;
	quat->y *= inv_sq;
	quat->z *= inv_sq;
}
/*----------------------------------------------------------------------------*/
void quat_inverse(quat_t* quat, quat_t* from) {
	float work = quat_product_dot(from);
	quat_conjugate(quat,from);
	quat_scale(quat,1.0f/(work*work));
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1QUATERNION__ */
/*----------------------------------------------------------------------------*/
