/*----------------------------------------------------------------------------*/
#include <GL/glut.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
/*----------------------------------------------------------------------------*/
#include "do3d_test.h"
/*----------------------------------------------------------------------------*/
do3d_test_t my3d;
/*----------------------------------------------------------------------------*/
#define VIEW_WIDTH 800
#define VIEW_HEIGHT 600
#define ANGLE_DELTA 5.0f
#define MOVE_DELTA 0.1f
/*----------------------------------------------------------------------------*/
void key_viewer(int key, int x, int y) {
	int axis, flag = 1;
	float diff = ANGLE_DELTA;
	switch (key) {
		/* rotate viewers's view */
		case GLUT_KEY_RIGHT: diff = -diff; /* no break here! */
		case GLUT_KEY_LEFT: axis = _3DXS_Y; break;
		case GLUT_KEY_DOWN: diff = -diff; /* no break here! */
		case GLUT_KEY_UP : axis = _3DXS_X; break;
		case GLUT_KEY_PAGE_DOWN: diff = -diff; /* no break here! */
		case GLUT_KEY_PAGE_UP : axis = _3DXS_Z; break;
		default: /**printf("-- Spec. key %c == %d\n",key,key);*/ flag = 0;
	}
	if (flag) {
		do3d_view_rotate(&my3d.view,axis,diff);
		glutPostRedisplay();
	}
}
/*----------------------------------------------------------------------------*/
void key_object(unsigned char key, int x, int y) {
	switch (key) {
		/* translate viewer position */
		case '+': do3d_view_move(&my3d.view,_3DXS_Z,-MOVE_DELTA);  break;
		case '-': do3d_view_move(&my3d.view,_3DXS_Z,MOVE_DELTA);  break;
		case 'w': do3d_view_move(&my3d.view,_3DXS_Y,-MOVE_DELTA);  break;
		case 's': do3d_view_move(&my3d.view,_3DXS_Y,MOVE_DELTA);  break;
		case 'd': do3d_view_move(&my3d.view,_3DXS_X,-MOVE_DELTA);  break;
		case 'a': do3d_view_move(&my3d.view,_3DXS_X,MOVE_DELTA);  break;
		/* translate object from current origin */
		case 'i': do3d_item_move(&my3d.cube,_3DXS_Y,MOVE_DELTA);  break;
		case 'k': do3d_item_move(&my3d.cube,_3DXS_Y,-MOVE_DELTA);  break;
		case 'l': do3d_item_move(&my3d.cube,_3DXS_X,MOVE_DELTA);  break;
		case 'j': do3d_item_move(&my3d.cube,_3DXS_X,-MOVE_DELTA);  break;
		case '[': do3d_item_move(&my3d.cube,_3DXS_Z,MOVE_DELTA);  break;
		case ']': do3d_item_move(&my3d.cube,_3DXS_Z,-MOVE_DELTA);  break;
		/* rotate object around local origin */
		case 'r': do3d_item_rotate(&my3d.cube,_3DXS_X,ANGLE_DELTA); break;
		case 'f': do3d_item_rotate(&my3d.cube,_3DXS_X,-ANGLE_DELTA); break;
		case 't': do3d_item_rotate(&my3d.cube,_3DXS_Y,ANGLE_DELTA); break;
		case 'g': do3d_item_rotate(&my3d.cube,_3DXS_Y,-ANGLE_DELTA); break;
		case 'y': do3d_item_rotate(&my3d.cube,_3DXS_Z,ANGLE_DELTA); break;
		case 'h': do3d_item_rotate(&my3d.cube,_3DXS_Z,-ANGLE_DELTA); break;
		/* reset everything */
		case 'o': do3d_test_redo(&my3d); break;
		/* ESC to exit */
		case 27: exit(0);
		/**default: printf("-- Keyboard %c == %d\n",key,key);*/
	}
}
/*----------------------------------------------------------------------------*/
void redraw(void) {
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	do3d_test_draw(&my3d);
	glutSwapBuffers();
}
/*----------------------------------------------------------------------------*/
void resize(GLsizei width, GLsizei height) {
	GLfloat aspect;
	if (height == 0) height = 1;
	aspect = (GLfloat)width / (GLfloat)height;
	glViewport(0,0,width,height);
	glPushMatrix();
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	/** perspective projection with fovy, aspect, zNear and zFar */
	gluPerspective(60.0f, aspect, 1.0f, 20.0f);
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}
/*----------------------------------------------------------------------------*/
int main(int argc, char *argv[]) {
	glutInit(&argc,argv);
	glutInitWindowSize(VIEW_WIDTH,VIEW_HEIGHT);
	glutInitDisplayMode(GLUT_DEPTH|GLUT_DOUBLE);
	glutCreateWindow("Object View");
	glutDisplayFunc(redraw);
	glutReshapeFunc(resize);
	glutKeyboardFunc(key_object);
	glutSpecialFunc(key_viewer);
	/**glutIdleFunc(do_something?);*/
	do3d_test_init(&my3d);
	glutMainLoop();
	return 0;
}
/*----------------------------------------------------------------------------*/
