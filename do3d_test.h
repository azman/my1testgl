/*----------------------------------------------------------------------------*/
#ifndef __MY1WORK3D_TEST__
#define __MY1WORK3D_TEST__
/*----------------------------------------------------------------------------*/
#include "do3d.h"
/*----------------------------------------------------------------------------*/
void do3d_item_draw_cube(do3d_item_t* item) {
	glBegin (GL_QUADS);
	glColor3f( 0.0,  0.7, 0.1); /* front - green */
	glVertex3f(-1.0,  1.0, 1.0);
	glVertex3f( 1.0,  1.0, 1.0);
	glVertex3f( 1.0, -1.0, 1.0);
	glVertex3f(-1.0, -1.0, 1.0);
	glColor3f( 0.9,  1.0,  0.0); /* rear  - yellow */
	glVertex3f(-1.0,  1.0, -1.0);
	glVertex3f( 1.0,  1.0, -1.0);
	glVertex3f( 1.0, -1.0, -1.0);
	glVertex3f(-1.0, -1.0, -1.0);
	glColor3f( 0.2, 0.2,  1.0); /* top - blue */
	glVertex3f(-1.0, 1.0,  1.0);
	glVertex3f( 1.0, 1.0,  1.0);
	glVertex3f( 1.0, 1.0, -1.0);
	glVertex3f(-1.0, 1.0, -1.0);
	glColor3f( 0.7,  0.0,  0.1); /* bottom - red */
	glVertex3f(-1.0, -1.0,  1.0);
	glVertex3f( 1.0, -1.0,  1.0);
	glVertex3f( 1.0, -1.0, -1.0);
	glVertex3f(-1.0, -1.0, -1.0);
	glColor3f(0.0, 0.9,  1.0); /* left */
	glVertex3f(-1.0, 1.0,  1.0);
	glVertex3f(-1.0,-1.0,  1.0);
	glVertex3f(-1.0,-1.0, -1.0);
	glVertex3f(-1.0, 1.0, -1.0);
	glColor3f(1.0, 0.0,  0.9); /* right */
	glVertex3f( 1.0,-1.0,  1.0);
	glVertex3f( 1.0, 1.0,  1.0);
	glVertex3f( 1.0, 1.0, -1.0);
	glVertex3f( 1.0,-1.0, -1.0);
	glEnd();
}
/*----------------------------------------------------------------------------*/
void do3d_item_draw_axis_common(do3d_item_t* item) {
	vec3_t temp, init = { 0.0f, 0.0f, 0.0f };
	glColor3f(1.0,1.0,1.0);
	glBegin(GL_LINES);
	/* x axis */
	vec3_fill(&temp,item->axis_size,0.0f,0.0f);
	glVertex3fv((float*)&init);
	glVertex3fv((float*)&temp);
	/* y axis */
	vec3_fill(&temp,0.0f,item->axis_size,0.0f);
	glVertex3fv((float*)&init);
	glVertex3fv((float*)&temp);
	/* z axis */
	vec3_fill(&temp,0.0f,0.0f,item->axis_size);
	glVertex3fv((float*)&init);
	glVertex3fv((float*)&temp);
	glEnd();
	/* draw axis labels */
	glRasterPos3f(item->axis_size*1.05, 0.0, 0.0);
	glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, 'X');
	glRasterPos3f(0.0, item->axis_size*1.05, 0.0);
	glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, 'Y');
	glRasterPos3f(0.0, 0.0, item->axis_size*1.05);
	glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, 'Z');
}
/*----------------------------------------------------------------------------*/
typedef struct _do3d_test_t {
	do3d_view_t view;
	do3d_item_t cube;
} do3d_test_t;
/*----------------------------------------------------------------------------*/
void do3d_test_init(do3d_test_t* test) {
	do3d_view_init(&test->view);
	do3d_item_init(&test->cube);
	test->cube.draw_item = do3d_item_draw_cube;
	test->cube.draw_axis = do3d_item_draw_axis_common;
	/* init opengl stuff */
	glClearColor(0.0f,0.0f,0.0f,1.0f); /* background is opaque black */
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);
}
/*----------------------------------------------------------------------------*/
void do3d_test_redo(do3d_test_t* test) {
	do3d_view_redo(&test->view);
	do3d_item_redo(&test->cube);
	glutPostRedisplay();
}
/*----------------------------------------------------------------------------*/
void do3d_test_draw(do3d_test_t* test) {
	/* translate/rotate view? */
	do3d_view_make(&test->view);
	/* draw object */
	do3d_item_make(&test->cube);
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1WORK3D_DATA__ */
/*----------------------------------------------------------------------------*/
